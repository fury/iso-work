#!/bin/sh

efi_path="efi/EFI/boot/bootx64.efi"
theme=""

usage (){
  echo "Usage:"
  echo "  -t: theme name"
  echo "  -e: efi path"
}

while getopts ":t:e:" opt; do
  case $opt in
    t)
      theme=$OPTARG
      ;;
    e)
      efi_path=$OPTARG
      ;;
    \?)
      usage
      exit 0
      ;;
    *)
      echo "Invalid Opriton" >&2
      usage
      exit 1
      ;;
  esac
done

grub-kbdcomp -o en.gkb us
grub-mkstandalone -d /usr/lib/grub/x86_64-efi/ -O x86_64-efi --fonts="/boot/grub2/fonts/unicode.pf2" --themes=$theme -o $efi_path "boot/grub/grub.cfg=./grub.cfg" -v
